package com.example.demo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;

@Repository
public interface FooRepository extends PagingAndSortingRepository<FooEntity, Integer> {
    
    @Query(value = "select p from foo p where name = :name")
    List<FooEntity> listByName(@Param("name") String name);

}


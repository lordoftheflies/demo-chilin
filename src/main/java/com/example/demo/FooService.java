package com.example.demo;

import org.jvnet.hk2.annotations.Service;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

@Service
public class FooService {

    @Autowired
    private FooRepository fooRepository;

    public List<FooEntity> listByName(String name) {
        return this.fooRepository.listByName(name);
    }
}
